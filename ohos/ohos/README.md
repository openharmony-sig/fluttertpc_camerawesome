# camerawesome_ohos

Easiest Flutter camera Plugin supporting capturing images,
streaming images, video recording, switch sensors, autofocus, flash... on
Android、IOS and HarmonyOS

### Usage:

```
  yaml
    dependencies:
      camerawesome: 1.4.0
      camerawesome_ohos: 1.0.0
```

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.